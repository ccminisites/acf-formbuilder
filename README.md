# ACF Form Builder Field

Welcome to the Advanced Custom Fields Form Builder repository on Github.

Let's you create forms as a field in ACF.

```
cd wp-content/plugins
bash <(wget -qO- https://gitlab.com/ccminisites/acf-formbuilder/-/raw/main/download.sh)
```
