<?php

/*
Plugin Name: Advanced Custom Fields: Form Builder
Plugin URI: https://www.contentcoffee.com
Description: Let's you create forms as a field in ACF
Version: 1.0.0
Author: ContentCoffee
Author URI: https://www.contentcoffe.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('cc_acf_plugin_formbuilder') ) :

class cc_acf_plugin_formbuilder {
	
	// vars
	var $settings;
	
	
	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	void
	*  @return	void
	*/
	
	function __construct() {
		
		// settings
		// - these will be passed into the field class.
		$this->settings = array(
			'version'	=> '1.0.0',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);
		
		
		// include field
		add_action('acf/include_field_types', 	array($this, 'include_field')); // v5
		add_action('acf/register_fields', 		array($this, 'include_field')); // v4
	}
	
	
	/*
	*  include_field
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	void
	*/
	
	function include_field( $version = false ) {
		
		// support empty $version
		if( !$version ) $version = 4;
		
		
		// load textdomain
		load_plugin_textdomain( 'ccformbuilder', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );
		
		
		// include
		include_once('fields/class-cc-acf-field-formbuilder-v' . $version . '.php');
	}
	
}


// initialize
new cc_acf_plugin_formbuilder();


// class_exists check
endif;

/**
 * Add scripts and styles just in the admin.
 */
function acf_formbuilder_enqueue_scripts()
{
    wp_enqueue_script('acf_formrender', get_home_url() . '/wp-content/plugins/acf-formbuilder/form-builder/form-render.min.js', [], false, true);
}

add_action('wp_enqueue_scripts', 'acf_formbuilder_enqueue_scripts');
