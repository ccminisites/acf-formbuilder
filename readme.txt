=== Advanced Custom Fields: Form Builder Field ===
Contributors: ContentCoffee
Tags: forms
Requires at least: 3.6.0
Tested up to: 5.9.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Let's you create forms as a field in ACF.

== Description ==

Creates a field that is a form field that will use form builder js.

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

== Installation ==

1. Copy the `acf-formbuilder` folder into your `wp-content/plugins` folder
2. Activate the Form Builder plugin via the plugins admin page
3. Create a new field via ACF and select the Form Builder type
4. Read the description above for usage instructions

== Changelog ==

= 1.0.0 =
* Initial Release.